package Tracking_packet;

import com.altirnao.aodocs.custom.*;
import com.altirnao.aodocs.custom.action.UpdateAction;
import com.altirnao.aodocs.custom.googleintegration.ContactService;
import com.altirnao.aodocs.custom.googleintegration.DriveService;
import com.altirnao.aodocs.custom.googleintegration.SpreadsheetService;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.repackaged.com.google.api.client.http.HttpResponse;
import org.json.JSONObject;

import java.util.*;

import static com.altirnao.aodocs.custom.Attachment.MIMETYPE_DOCUMENT;

public abstract class MainScript implements UpdateAction {
    public void update(ReadableDocument document) throws ValidationException, Exception {
        if(!document.isDeleted() && document.getAttachments().isEmpty()){
            // Set title
            Date date = new Date();
            String year = String.valueOf(date.getYear() + 1900);
            String month = String.valueOf(date.getMonth() + 1);
            String day = String.valueOf(date.getDate());
            String hour = String.valueOf(date.getHours() + 1);
            String minutes = String.valueOf(date.getMinutes());
            if (month.length() < 2) month = "0" + month;
            if (day.length() < 2) day = "0" + day;
            if (hour.length() < 2) hour = "0" + hour;
            if (minutes.length() < 2) minutes = "0" + minutes;
            String title = year + month + day + hour + minutes;
            Document lockDocument = getDocumentService().lockDocument(document);
            if (document.getTitle().equals(getParam("no_title"))) {
                lockDocument.setTitle(title);
            }

            //add attachement on this document
            DocumentService documentService = this.getDocumentService();
            Attachment attachment = documentService.createAttachment("Fiche de suivie numéro: " + document.getTitle().toString(), MIMETYPE_DOCUMENT, null);
            lockDocument.addAttachment(attachment);

            //check if dest is null
            String dest;
            String commentaire ="";
            if(document.getField(getParam("dest")) == null){
                if(document.getRichText() == "") {
                    throw new ValidationException("Veuillez renseigner le destinataire ou le nom de celui-ci dans la description");
                }
                else{
                    commentaire = document.getRichText();
                    dest = "servcoud@essilor.fr";
                    lockDocument.setField(getParam("dest"),dest);
                }
            }
            else {
                dest = document.getField(getParam("dest")).toString();
            }

            //Fill json to POST
            JSONObject json = new JSONObject();
            json.put("docId", attachment.getFileId());
            json.put("date", day + "/" + month + "/" + year );
            json.put("num", document.getTitle().toString());
            json.put("destinataire", dest);
            json.put("commentaire", commentaire);
            json.put("transporteur", document.getField(getParam("transp")).toString());
            json.put("tracking", document.getField(getParam("track")).toString());
            json.put("commande", document.getField(getParam("comm")).toString());
            json.put("colisnb", document.getField(getParam("nb_col")).toString());
            json.put("colispoids", document.getField(getParam("pd_col")).toString());
            json.put("livraison", document.getHierarchicalCategory(getParam("type_col")).get(1).toString());
            json.put("fournisseur", document.getField(getParam("fourn")).toString());
            String data = json.toString().substring(1, json.toString().length() - 1);
            //debug(data);



            //Send data and receive response
            FetchOptions options = FetchOptions.Builder.withDefaults();
            options.setDeadline(60000.0);

            Thread.sleep(2000);

            HTTPResponse resp = getUrlFetchService().fetch(getParam("backend"), HTTPMethod.POST, ("entry.802584520=" + data).getBytes("UTF-8"), null, options);
            String content = new String(resp.getContent(), "UTF-8");
            debug(content);

            //Change state of document
            if(document.getState().equals("Email à envoyer"))
                lockDocument.setState(getParam("send_email"));
        }
    }

    public String getId() {
        return null;
    }

    public String getName() {
        return null;
    }

    public String getApplicationId() {
        return null;
    }

    public String getApplicationVersion() {
        return null;
    }

    public String getDomain() {
        return null;
    }

    public String getLibraryId() {
        return null;
    }

    public String getLibraryName() {
        return null;
    }

    public Long getLibrarySequenceId() {
        return null;
    }

    public String getParam(String paramName) {
        return null;
    }

    public DocumentService getDocumentService() {
        return null;
    }

    public ContactService getContactService() {
        return null;
    }

    public DriveService getDriveService() {
        return null;
    }

    public SpreadsheetService getSpreadsheetService() {
        return null;
    }

    public FeedbackHelper getFeedbackHelper() {
        return null;
    }

    public PermissionService getPermissionService() {
        return null;
    }

    public EmailService getEmailService() {
        return null;
    }

    public CategoryService getCategoryService() {
        return null;
    }

    public UrlFetchService getUrlFetchService() {
        return null;
    }

    public void debug(String msg) {

    }

    public void debug(String msg, Throwable thrown) {

    }

    public void error(String msg) {

    }

    public void error(String msg, Throwable thrown) {

    }
}
