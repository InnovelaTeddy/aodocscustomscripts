package Dashboard;

import com.altirnao.aodocs.custom.*;
import com.altirnao.aodocs.custom.action.UpdateAction;
import com.altirnao.aodocs.custom.googleintegration.ContactService;
import com.altirnao.aodocs.custom.googleintegration.DriveService;
import com.altirnao.aodocs.custom.googleintegration.SpreadsheetService;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPResponse;
import org.json.JSONObject;

public abstract class CheckRequest implements UpdateAction{
    public void update(ReadableDocument document) throws ValidationException, Exception {
        DocumentService documentService = this.getDocumentService();
        if(!document.isDeleted() && document.getState().equals("NOT STARTED")){
            String message = " ";
            /*
             * Test if library name exists
             */
            String content = "lib="+document.getField(getParam("name"));
            byte[] data = content.getBytes("UTF-8");
            FetchOptions options = FetchOptions.Builder.withDefaults();
            options.setDeadline(60000.0);
            HTTPResponse httpResponse = getUrlFetchService().fetch(getParam("getLib"), HTTPMethod.POST, data, null, options);
            byte[] bytesResponse = httpResponse.getContent();
            String response = new String(bytesResponse, "UTF-8");
            JSONObject json = new JSONObject(response);
            if(json.getBoolean("message"))
                { throw new ValidationException("The name of the library already exists, please change the librarie's name");
            }

            /*
             * Test if capacity exceed max capacity
             */
            int capacity = Integer.parseInt(document.getField(getParam("capacity")).toString());
            int maxCapacity = Integer.parseInt(getParam("maxCapacity"));

            if(capacity>=maxCapacity)
                message += "We asked a library with more than 30GB. An extension will be added between 5 and 10 days. ";
            else
                message += "We asked a library with less than 30GB. If you exceed 30GB an extension will be automatically added. The bill will be reduced. ";

            /*
             * Test if capacity exceed max number of document
             */
            int nbDoc = Integer.parseInt(document.getField(getParam("nbDoc")).toString());
            int maxNbDoc = Integer.parseInt(getParam("maxNbDoc"));

            if(nbDoc>=maxNbDoc){
                //create a denied document
                Document d = documentService.createDocument("Denied",document.getTitle());
                d.setField(getParam("name"),document.getField(getParam("name")).toString());
                d.setField(getParam("section"),document.getField(getParam("section")).toString());
                d.setField(getParam("mess"),message);
                d.setField(getParam("capacity"),Integer.parseInt(document.getField(getParam("capacity")).toString()));
                d.setField(getParam("nbDoc"), Integer.parseInt(document.getField(getParam("nbDoc")).toString()));
                d.setField(getParam("migration"),Boolean.valueOf(document.getField(getParam("migration")).toString()));
                d.setField(getParam("fileServer"),Boolean.valueOf(document.getField(getParam("fileServer")).toString()));
                d.setField(getParam("requestor"),document.getField(getParam("requestor")).toString());
                String num = String.format("%07d",d.getSequenceId());
                d.setField("DeniedNumber",num);
                documentService.store(d);
                throw new ValidationException("The number of documents expected not allow an automatic creation, please contact the Run ( transmit this number:"+num+")");
            }

            /*
             * Test if user need help to migrate his document
             */
            boolean migration = Boolean.valueOf(document.getField(getParam("migration")).toString());
            if(migration){
                //create a denied document
                Document d = documentService.createDocument("Denied",document.getTitle());
                d.setField(getParam("name"),document.getField(getParam("name")).toString());
                d.setField(getParam("section"),document.getField(getParam("section")).toString());
                d.setField(getParam("mess"),message);
                d.setField(getParam("capacity"),Integer.parseInt(document.getField(getParam("capacity")).toString()));
                d.setField(getParam("nbDoc"),Integer.parseInt(document.getField(getParam("nbDoc")).toString()));
                d.setField(getParam("migration"),Boolean.valueOf(document.getField(getParam("migration")).toString()));
                d.setField(getParam("fileServer"),Boolean.valueOf(document.getField(getParam("fileServer")).toString()));
                d.setField(getParam("requestor"),document.getField(getParam("requestor")).toString());
                String num = String.format("%07d",d.getSequenceId());
                d.setField("DeniedNumber",num);
                documentService.store(d);
                throw new ValidationException("The user needs help to migrate his document, please contact the Run ( transmit this number:"+num+")");
            }

            /*
             * Test if user will use AoDocs like a file server
             */
            boolean fileServer = Boolean.valueOf(document.getField(getParam("fileServer")).toString());
            if(fileServer){
                //create a denied document
                Document d = documentService.createDocument("Denied",document.getTitle());
                d.setField(getParam("name"),document.getField(getParam("name")).toString());
                d.setField(getParam("section"),document.getField(getParam("section")).toString());
                d.setField(getParam("mess"),message);
                d.setField(getParam("capacity"),Integer.parseInt(document.getField(getParam("capacity")).toString()));
                d.setField(getParam("nbDoc"),Integer.parseInt(document.getField(getParam("nbDoc")).toString()));
                d.setField(getParam("migration"),Boolean.valueOf(document.getField(getParam("migration")).toString()));
                d.setField(getParam("fileServer"),Boolean.valueOf(document.getField(getParam("fileServer")).toString()));
                d.setField(getParam("requestor"),document.getField(getParam("requestor")).toString());
                String num = String.format("%07d",d.getSequenceId());
                d.setField("DeniedNumber",num);
                documentService.store(d);
                throw new ValidationException("The user would like a specifical library, please contact the Run ( transmit this number:"+num+")");
            }
            Document lockDocument = getDocumentService().lockDocument(document);
            lockDocument.setState("IN PROGRESS");
            lockDocument.setField(getParam("mess").toString(),message);
        }
    }

    public String getId() {
        return null;
    }

    public String getName() {
        return null;
    }

    public String getApplicationId() {
        return null;
    }

    public String getApplicationVersion() {
        return null;
    }

    public String getDomain() {
        return null;
    }

    public String getLibraryId() {
        return null;
    }

    public String getLibraryName() {
        return null;
    }

    public Long getLibrarySequenceId() {
        return null;
    }

    public String getParam(String paramName) {
        return null;
    }

    public DocumentService getDocumentService() {
        return null;
    }

    public ContactService getContactService() {
        return null;
    }

    public DriveService getDriveService() {
        return null;
    }

    public SpreadsheetService getSpreadsheetService() {
        return null;
    }

    public FeedbackHelper getFeedbackHelper() {
        return null;
    }

    public PermissionService getPermissionService() {
        return null;
    }

    public EmailService getEmailService() {
        return null;
    }

    public CategoryService getCategoryService() {
        return null;
    }

    public UrlFetchService getUrlFetchService() {
        return null;
    }

    public void debug(String msg) {

    }

    public void debug(String msg, Throwable thrown) {

    }

    public void error(String msg) {

    }

    public void error(String msg, Throwable thrown) {

    }
}
