package Dashboard;

import com.altirnao.aodocs.custom.*;
import com.altirnao.aodocs.custom.action.BackendAction;
import com.altirnao.aodocs.custom.googleintegration.ContactService;
import com.altirnao.aodocs.custom.googleintegration.DriveService;
import com.altirnao.aodocs.custom.googleintegration.SpreadsheetService;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPResponse;
import org.json.JSONObject;

public abstract class SendRequestToBackend implements BackendAction {
    public void execute(ReadableDocument document) throws ValidationException, Exception {
        int capacity = Integer.parseInt(document.getField(getParam("capacity")).toString());
        int maxCapacity = Integer.parseInt(getParam("maxCapacity"));
        Boolean extension = false;
        if(capacity>=maxCapacity){
            if(capacity>30) extension = true;
        }

        JSONObject json = new JSONObject();
        json.put("library_name",document.getField(getParam("lib")));
        json.put("requestor",document.getField(getParam("mail")));
        json.put("library_type",getParam("type"));
        json.put("capacity",capacity);
        json.put("extension",extension);
        debug(json.toString());
        FetchOptions options = FetchOptions.Builder.withDefaults();
        options.setDeadline(60000.0);

        HTTPResponse httpResponse = getUrlFetchService().fetch(getParam("backend"), HTTPMethod.POST, json.toString().getBytes(), null, options);
        Document lockDocument = getDocumentService().lockDocument(document);
        byte[] bytesResponse = httpResponse.getContent();
        String response = new String(bytesResponse, "UTF-8");
        if(httpResponse.getResponseCode() == 200) {
            json = new JSONObject(response);
            String libraryId = json.getString("library_id");
            String group = json.getString("group");
            debug(libraryId);
            debug(group);
            lockDocument.setState(getParam("created"));
            lockDocument.setField(getParam("libid"),libraryId);
            lockDocument.setField(getParam("group"),group);
        }
        else{
            debug(response);
            lockDocument.setState(getParam("not_started"));
        }
    }

    public String getId() {
        return null;
    }

    public String getName() {
        return null;
    }

    public String getApplicationId() {
        return null;
    }

    public String getApplicationVersion() {
        return null;
    }

    public String getDomain() {
        return null;
    }

    public String getLibraryId() {
        return null;
    }

    public String getLibraryName() {
        return null;
    }

    public Long getLibrarySequenceId() {
        return null;
    }

    public String getParam(String paramName) {
        return null;
    }

    public DocumentService getDocumentService() {
        return null;
    }

    public ContactService getContactService() {
        return null;
    }

    public DriveService getDriveService() {
        return null;
    }

    public SpreadsheetService getSpreadsheetService() {
        return null;
    }

    public FeedbackHelper getFeedbackHelper() {
        return null;
    }

    public PermissionService getPermissionService() {
        return null;
    }

    public EmailService getEmailService() {
        return null;
    }

    public CategoryService getCategoryService() {
        return null;
    }

    public UrlFetchService getUrlFetchService() {
        return null;
    }

    public void debug(String msg) {

    }

    public void debug(String msg, Throwable thrown) {

    }

    public void error(String msg) {

    }

    public void error(String msg, Throwable thrown) {

    }
}
