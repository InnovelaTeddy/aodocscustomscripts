package immo;

import com.altirnao.aodocs.custom.*;
import com.altirnao.aodocs.custom.action.UpdateAction;
import com.altirnao.aodocs.custom.googleintegration.ContactService;
import com.altirnao.aodocs.custom.googleintegration.DriveService;
import com.altirnao.aodocs.custom.googleintegration.SpreadsheetService;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPResponse;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class setTitleAndCreateFolder implements UpdateAction {

    public void update(ReadableDocument document) throws ValidationException, Exception {
        if(document.getHierarchicalCategory(getParam("continent")).size()< 5) {
            throw new ValidationException(getParam("message_continent"));
        }

        if(document.getHierarchicalCategory(getParam("continent")).size() == 6 && document.getField(getParam("dossier")).toString().equals(getParam("dossier_general_data"))){
            throw new ValidationException(getParam("message_general_data_on_batiment"));
        }

        if(document.getHierarchicalCategory(getParam("continent")).size() == 5 && !document.getField(getParam("dossier")).toString().equals(getParam("dossier_general_data"))){
            throw new ValidationException(getParam("message_other_on_adresse"));
        }


        Document lockDocument = getDocumentService().lockDocument(document);
        String pays = document.getHierarchicalCategory(getParam("continent")).get(1).toString();

        //Get the bigramme of country
        FetchOptions options = FetchOptions.Builder.withDefaults();
        options.setDeadline(60000.0);
        HTTPResponse httpResponse = getUrlFetchService().fetch(getParam("backend_country")+""+pays, HTTPMethod.GET, null, null, options);
        byte[] bytesResponse = httpResponse.getContent();
        String response = new String(bytesResponse, "UTF-8");
        debug(response);
        String paysShort = "";
        if(httpResponse.getResponseCode() == 200) {
            JSONObject json = new JSONObject(response);
            paysShort = json.getString("message");
        }
        else
            throw new ValidationException(getParam("message_no_bigramme"));

        String continent = document.getField(getParam("continent")).toString();
        String ville = document.getHierarchicalCategory(getParam("continent")).get(2).toString();
        String zip = document.getHierarchicalCategory(getParam("continent")).get(3).toString();
        String adresse = document.getHierarchicalCategory(getParam("continent")).get(4).toString();
        String site = "";
        if(document.getHierarchicalCategory(getParam("continent")).size() > 5) {
            site = document.getHierarchicalCategory(getParam("continent")).get(5).toString();
        }
        String contenu = document.getField(getParam("contenu")).toString();
        String dossier = document.getField(getParam("dossier")).toString();

        //Set title
        if (document.getTitle().equals(getParam("no_title"))) {
            String title = "";
            Date d = new Date(document.getField(getParam("date")).toString());
            String year = String.valueOf(d.getYear() + 1900);
            String month = String.valueOf(d.getMonth() + 1);
            if (month.length() < 2) month = "0" + month;
            String date = year + month;
            if(site != "") {
                String smallSite = site;
                if(site.length() >= 3) smallSite = site.substring(0, 3);
                title = continent.substring(0, 2).toUpperCase() + "_" +
                        paysShort + "_" +
                        ville.substring(0, 2).toUpperCase() + "_" +
                        smallSite.toUpperCase() + "_" +
                        dossier + "_" +
                        date + "_" +
                        contenu;
            }
            else{
                title = continent.substring(0, 2).toUpperCase() + "_" +
                        pays.substring(0, 2).toUpperCase() + "_" +
                        ville.substring(0, 2).toUpperCase() + "_" +
                        dossier + "_" +
                        date + "_" +
                        contenu;
            }
            debug(title);
            lockDocument.setTitle(title);
        }

        //Set Folder
        List<String> targetFolder = new ArrayList<String>();
        targetFolder.add(continent);
        targetFolder.add(paysShort);
        targetFolder.add(ville+ " - " +zip);
        targetFolder.add(adresse);
        if(site != "") targetFolder.add(site);
        targetFolder.add(dossier);
        if(document.getHierarchicalCategory(getParam("dossier")).size() > 1)
            targetFolder.add(document.getHierarchicalCategory(getParam("dossier")).get(1).toString());

        String lastFolder = targetFolder.get(targetFolder.size()-1).toString();

        List<String> toCreate = new ArrayList<String>();
        for (String folder: targetFolder){
            if(folder != lastFolder) toCreate.add(folder);
        }
        getCategoryService().createCategoryValue("Folder",toCreate ,lastFolder,"",true);
        lockDocument.setHierarchicalCategory(getParam("folder"),targetFolder);
        debug("Document classified");
    }

    public String getId() {
        return null;
    }

    public String getName() {
        return null;
    }

    public String getApplicationId() {
        return null;
    }

    public String getApplicationVersion() {
        return null;
    }

    public String getDomain() {
        return null;
    }

    public String getLibraryId() {
        return null;
    }

    public String getLibraryName() {
        return null;
    }

    public Long getLibrarySequenceId() {
        return null;
    }

    public String getParam(String paramName) {
        return null;
    }

    public DocumentService getDocumentService() {
        return null;
    }

    public ContactService getContactService() {
        return null;
    }

    public DriveService getDriveService() {
        return null;
    }

    public SpreadsheetService getSpreadsheetService() {
        return null;
    }

    public FeedbackHelper getFeedbackHelper() {
        return null;
    }

    public PermissionService getPermissionService() {
        return null;
    }

    public EmailService getEmailService() {
        return null;
    }

    public CategoryService getCategoryService() {
        return null;
    }

    public UrlFetchService getUrlFetchService() {
        return null;
    }

    public void debug(String msg) {

    }

    public void debug(String msg, Throwable thrown) {

    }

    public void error(String msg) {

    }

    public void error(String msg, Throwable thrown) {

    }
}
