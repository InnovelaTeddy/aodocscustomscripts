package IP;

import com.altirnao.aodocs.custom.ReadableDocument;
import com.altirnao.aodocs.custom.ValidationException;
import com.altirnao.aodocs.custom.action.BackendAction;
import com.altirnao.aodocs.custom.googleintegration.ContactService;
import com.altirnao.aodocs.custom.googleintegration.DriveService;
import com.altirnao.aodocs.custom.googleintegration.SpreadsheetService;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPResponse;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class MarkLib implements BackendAction {
    public void execute(ReadableDocument document) throws ValidationException, Exception {
        if(document.getField(getParam("tm_name")) != null && !document.isDeleted()){
            String email = getParam("email");
            debug(email);
            String date = "";
            String applicant = "";
            List<String> country = new ArrayList<String>();
            String filing_type = "";
            String event_tm = "";

            //Check fields to labels
            if (document.getField(getParam("applicant")) != null)
                applicant = document.getField(getParam("applicant")).toString();
            if (document.getHierarchicalMultiCategory(getParam("country")) != null) {
                List<List<String>> c = document.getHierarchicalMultiCategory(getParam("country"));
                for(int i = 0; i < c.size(); i++){
                    String value = c.get(i).toString();
                    value = value.split("-")[1];
                    value = value.substring(0, value.length()-1);
                    country.add(value);
                }
                debug(country.toString());
            }
            if (document.getField(getParam("filing_type")) != null)
                filing_type = document.getField(getParam("filing_type")).toString();
            if (document.getField(getParam("event_tm")) != null)
                event_tm = document.getField(getParam("event_tm")).toString();

            //Get the subject of the email
            String title = document.getTitle().toString();
            String sujet = "NO Subject";
            if(title.contains(":")){
                String[] temp = title.split(":");
                String toBegin = "";
                if(temp.length >= 1) toBegin = temp[1];
                else toBegin = title;
                sujet = title.substring(title.indexOf(toBegin),title.length());
            }
            debug(sujet);

            /* Obtain the date of the email */
            String patternStr = "([A-Z][a-z]{2}, [0-9]{1,2} [A-Z][a-z]{2} [0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2} [^\\w][0-9]{4})";
            Pattern pattern = Pattern.compile(patternStr);
            Matcher matcher = pattern.matcher(document.getRichText());
            boolean matchFound = matcher.find();
            if (matchFound && matcher.groupCount()>=1) {
                date  = matcher.group(1);
            }
            if(date == "") {
                patternStr = "([0-9]{1,2} [A-Z][a-z]{2} [0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2} [^\\w][0-9]{4})";
                pattern = Pattern.compile(patternStr);
                matcher = pattern.matcher(document.getRichText());
                matchFound = matcher.find();
                if (matchFound && matcher.groupCount() >= 1) {
                    date = matcher.group(1);
                }
            }
            debug(date);

            //Fill json to POST
            JSONObject json = new JSONObject();
            json.put("email", email);
            json.put("head", getParam("label_head"));
            json.put("country", country);
            json.put("applicant", applicant);
            json.put("filing type", filing_type);
            json.put("event tm", event_tm);
            json.put("subject", sujet);
            json.put("date", date);

            //send request to wedoo backend
            FetchOptions options = FetchOptions.Builder.withDefaults();
            options.setDeadline(60000.0);
            HTTPResponse httpResponse = getUrlFetchService().fetch(getParam("backend"),
                    HTTPMethod.POST, json.toString().getBytes(), null, options);
            byte[] bytesResponse = httpResponse.getContent();
            String response = new String(bytesResponse, "UTF-8");
            debug(response);

        }
    }
}
