package IP;

import com.altirnao.aodocs.custom.*;
import com.altirnao.aodocs.custom.action.UpdateAction;
import com.altirnao.aodocs.custom.googleintegration.ContactService;
import com.altirnao.aodocs.custom.googleintegration.DriveService;
import com.altirnao.aodocs.custom.googleintegration.SpreadsheetService;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPResponse;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class CreateFolderOnDrive implements UpdateAction {

    public void update(ReadableDocument document) throws ValidationException, Exception {
        if(document.getField(getParam("metadata_cas_number")) != null && !document.isDeleted()){
            FetchOptions options = FetchOptions.Builder.withDefaults();
            options.setDeadline(60000.0);


            /*** For new version ***/
            String date = "";
            JSONObject json = new JSONObject();
            json.put("email", getParam("email"));
            json.put("head", getParam("label_head"));
            /*** End for New version ***/

            /* Test CAS Number */
            String casNumber = document.getField(getParam("metadata_cas_number")).toString();
            casNumber = casNumber.replace(" ","");
            String cas = "";
            String number = "";
            if(casNumber.length() >= 6) {
                cas = casNumber.substring(0, 3).toUpperCase();
                number = casNumber.substring(3, casNumber.length());
            }
            else
                throw new ValidationException("The CAS Number is not correct / Le CAS Number saisi est incorrect");

            String regexCas = "[a-zA-Z]";
            String regexNumber = "^[0-9]{3,4}";
            Pattern patternCas = Pattern.compile(regexCas);
            Pattern patternNumber = Pattern.compile(regexNumber);
            Matcher matcherCas = patternCas.matcher(cas);
            Matcher matcherNumber = patternNumber.matcher(number);
            boolean matchesCas = matcherCas.find();
            boolean matchesNumber = matcherNumber.find();
            if(!matchesCas || !matchesNumber) throw new ValidationException("The CAS Number is not correct / Le CAS Number saisi est incorrect");

            /* Get the number of the CAS Number */
            Integer casNumberInt = Integer.parseInt(number);

            /* Found the interval */
            Integer cent = 0;
            // if the CAS Number terminated by 00
            cent = (casNumberInt) / 100;
            String centString = cent.toString();
            Integer maxCas = new Integer(centString+"99").intValue();
            Integer minCas = maxCas - 99;
            String maxCasString = maxCas.toString();
            String minCasString = minCas.toString();

            //Always 4 digits
            while(maxCasString.length()<4){
                maxCasString = "0" + maxCasString;
            }
            //Always 4 digits
            while(minCasString.length()<4){
                minCasString = "0" + minCasString;
            }
            String firstFolder = "";
            String casFolder = cas +""+ number;
            if(casNumberInt < 1000 && cas.equals("CAS")) firstFolder = "CAS500 to CAS999";// Attendre CLP pour "CAS" + minCas.toString() + " to CAS" + maxCas.toString()
            else {
                firstFolder = cas +""+ minCasString + " to " + cas +""+ maxCasString;
                //Always 4 digits
                while (casFolder.length() < 7) {
                    casFolder = casFolder.replace(cas, cas + "0");
                }
            }

            /* Create arborescence */
            List<String> targetFolder = new ArrayList<String>();
            targetFolder.add(firstFolder);
            targetFolder.add(casFolder);
            if(document.getField(getParam("metadata_filing_type")) != null){
                /** For New Version **/
                json.put(getParam("metadata_filing_type"),
                        document.getField(getParam("metadata_filing_type")).toString());
                if(document.getField(getParam("metadata_country")) != null) {
                    targetFolder.add(document.getField(getParam("metadata_country")).toString()
                            + "-"
                            + document.getField(getParam("metadata_filing_type")).toString());
                    /** For New Version **/
                    json.put(getParam("metadata_country"),
                            document.getField(getParam("metadata_country")).toString());
                }
                else targetFolder.add(document.getField(getParam("metadata_filing_type")).toString());

            }
            else{
                if(document.getField(getParam("metadata_country")) != null) {
                    targetFolder.add(document.getField(getParam("metadata_country")).toString());
                    /** For New Version **/
                    json.put(getParam("metadata_country"),
                            document.getField(getParam("metadata_country")).toString());
                }
            }

            if(document.getField(getParam("metadata_folders")) != null) {
                targetFolder.add(document.getField(getParam("metadata_folders")).toString());
                /** For New Version **/
                json.put(getParam("metadata_folders"),
                        document.getField(getParam("metadata_folders")).toString());
            }

            if(document.getField(getParam("metadata_event_cpa_subfolder")) != null) {
                targetFolder.add(document.getField(getParam("metadata_event_cpa_subfolder")).toString());
                /** For New Version **/
                json.put(getParam("metadata_event_cpa_subfolder"),
                        document.getField(getParam("metadata_event_cpa_subfolder")).toString());
            }

            /* Move the document and set Folder field */
            String lastFolder = targetFolder.get(targetFolder.size()-1).toString();
            debug(targetFolder.toString());
            debug(lastFolder);
            List<String> toCreate = new ArrayList<String>();
            for (String folder_: targetFolder){
                if(folder_ != lastFolder) toCreate.add(folder_);
            }
            //debug(toCreate);

            Document lockDocument = getDocumentService().lockDocument(document);
            getCategoryService().createCategoryValue("Folder",toCreate ,lastFolder,"");
            lockDocument.setHierarchicalCategory( getParam("metadata_folder") , targetFolder );
            debug("Document classified");

            //Get the subject of the email
            String title = document.getTitle().toString();
            String sujet = "NO Subject";
            if(title.contains(":")){
                String[] temp = title.split(":");
                String toBegin = "";
                if(temp.length >= 1) toBegin = temp[1];
                else toBegin = title;
                sujet = title.substring(title.indexOf(toBegin),title.length());
            }
            debug(sujet);

            /**** For New Version ***/
            json.put("subject", sujet);

            /* Obtain the date of the email */
            String patternStr = "([A-Z][a-z]{2}, [0-9]{1,2} [A-Z][a-z]{2} [0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2} [^\\w][0-9]{4})";
            Pattern pattern = Pattern.compile(patternStr);
            Matcher matcher = pattern.matcher(document.getRichText());
            boolean matchFound = matcher.find();
            if (matchFound && matcher.groupCount()>=1) {
                date  = matcher.group(1);
            }
            if(date == "") {
                patternStr = "([0-9]{1,2} [A-Z][a-z]{2} [0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2} [^\\w][0-9]{4})";
                pattern = Pattern.compile(patternStr);
                matcher = pattern.matcher(document.getRichText());
                matchFound = matcher.find();
                if (matchFound && matcher.groupCount() >= 1) {
                    date = matcher.group(1);
                }
            }


            json.put("date", date);
            if(date != ""){
                //send request to wedoo backend
                HTTPResponse httpResponse = getUrlFetchService().fetch(getParam("backend"),
                        HTTPMethod.POST, json.toString().getBytes(), null, options);
                byte[] bytesResponse = httpResponse.getContent();
                String response = new String(bytesResponse, "UTF-8");
                debug(response);

                if(response.contains("400")){
                    json.put("email", getParam("email2"));
                    httpResponse = getUrlFetchService().fetch(getParam("backend"),
                            HTTPMethod.POST, json.toString().getBytes(), null, options);
                    bytesResponse = httpResponse.getContent();
                    response = new String(bytesResponse, "UTF-8");
                    debug(response);
                }

            }
            else debug("No mail attached at this document");
        }
        else{
            if(!document.isDeleted())debug("Metadata CAS Number not fill");
            else debug("Document was deleted");
        }
    }

    public String getId() {
        return null;
    }

    public String getName() {
        return null;
    }

    public String getApplicationId() {
        return null;
    }

    public String getApplicationVersion() {
        return null;
    }

    public String getDomain() {
        return null;
    }

    public String getLibraryId() {
        return null;
    }

    public String getLibraryName() {
        return null;
    }

    public Long getLibrarySequenceId() {
        return null;
    }

    public String getParam(String paramName) {
        return null;
    }

    public DocumentService getDocumentService() {
        return null;
    }

    public ContactService getContactService() {
        return null;
    }

    public DriveService getDriveService() {
        return null;
    }

    public SpreadsheetService getSpreadsheetService() {
        return null;
    }

    public FeedbackHelper getFeedbackHelper() {
        return null;
    }

    public PermissionService getPermissionService() {
        return null;
    }

    public EmailService getEmailService() {
        return null;
    }

    public CategoryService getCategoryService() {
        return null;
    }

    public UrlFetchService getUrlFetchService() {
        return null;
    }

    public void debug(String msg) {

    }

    public void debug(String msg, Throwable thrown) {

    }

    public void error(String msg) {

    }

    public void error(String msg, Throwable thrown) {

    }
}
