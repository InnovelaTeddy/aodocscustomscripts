package PCS;

import com.altirnao.aodocs.custom.*;
import com.altirnao.aodocs.custom.action.BackendAction;
import com.altirnao.aodocs.custom.googleintegration.ContactService;
import com.altirnao.aodocs.custom.googleintegration.DriveService;
import com.altirnao.aodocs.custom.googleintegration.SpreadsheetService;
import com.google.appengine.api.urlfetch.FetchOptions;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPResponse;
import org.json.JSONObject;

public abstract class CalculCost implements BackendAction {
    public void execute(ReadableDocument document) throws ValidationException, Exception {
        String ram_type = document.getField("RAM type").toString();
        String purchase_qtt = document.getField("Purchased quantity").toString();
        String purchase_rqt = document.getField("Purchased requested RAM").toString();
        debug(ram_type);
        debug(purchase_qtt);
        debug(purchase_rqt);
        JSONObject json = new JSONObject();
        json.put("ram_type",ram_type);
        json.put("purchase_rqt",purchase_rqt);
        FetchOptions options = FetchOptions.Builder.withDefaults();
        options.setDeadline(60000.0);
        HTTPResponse httpResponse = getUrlFetchService().fetch("https://wedoo-backend.appspot.com/api/aodocs/pcs/", HTTPMethod.POST, json.toString().getBytes(), null, options);
        String content = new String(httpResponse.getContent(), "UTF-8");
        debug(content);
    }
}
